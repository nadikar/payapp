# Payment Application #

This README would normally document whatever steps are necessary to get your application up and running.

### Assumptions ###

* Load data to the items and merchant done using the scripts run in the startup of the springboot app. (located in src/resources)
* The ApiUsername, password should come as Authorization header and handle by spring security in real world, but to keep the size of this assessment to minimal, did not go down that path.
* Decided to choose accuracy over performance by pessimistically locking merchant items, when validating whether an order can be fulfilled. However, in the real world we may accept the payment without checking the stock levels and refund (as completion payment)fully or partially if stocks are not available.
* Storing the transaction will be used for auditing purposes and therefore saving card (excluding sensitive) and order details as JSONs
* Only wrote few integration tests and few unit tests to demonstrate it can be done, but in real word at least critical methods will be covered by unit tests with positive and negative scenarios.
* Added "PayPalPaymentProcessor" to demonstrate, extensibility of the code and it is not functional.
* Database optimizations are excluded and therefore did not create indexes and other database optimizations.
* Introduced two profiles one for dev and one for test. dev profile has debug level logging, showing sqls and different database name.
* To improve the performance (when scaling) some database accesses performed in methods such as postProcess, handleFailure can be done asynchronously using Spring events or messaging.
* Payment Gateway responses simulated in CreditCardPaymentProcessor::pay method. When you pass a negative amount it returns failure.
* Assume maximum number of items er payment is 10. You can the change the hibernate batch size by changing spring.jpa.properties.hibernate.jdbc.batch_size to optimize the performance.
* Stored password in plain text for simplicity. Passwords will be stored in a directory server or encrypted format in the database in real word.

### prerequisite ###

* Maven
* Java 11

### How to compile? ###
* Go to <root-project-dir> and execute "mvn clean install"

### How to run? ###

Run As springboot fat jar with "dev" profile

* Go to <root-project-dir> and execute "java -Dspring.profiles.active=dev -jar target/payment-system-0.0.1-SNAPSHOT.jar"
* Payments can be preformed by POST to http://localhost:8080/paymentapp/transactions/payment
* Transactions can be retrieved by GET to http://localhost:8080/paymentapp/transactions/<transactionId>
* Simple postman project available in the <root-project-dir>\src\test\resources which can be imported to Postman to send payments and retrieve transaction
* You can connect to the in memory H2 database using http://localhost:8080/paymentapp/h2-console. Use connection string jdbc:h2:mem:dev;DB_CLOSE_DELAY=-1, if you are running as dev profile. Note that db name is "dev" for dev profile and "test" for test profile

Running the App in IDE(intellij)
* If you running as an Application run the PaymentSystemApplication.java with profile set to dev (Standalone app VM Options = -Dspring.profiles.active=dev )


### Troubleshooting ###
* If you are keep getting 'UNAUTHORIZED' error, chances are that the data scripts did not work. Ensure that you have specified the profile value ("dev" or "test")