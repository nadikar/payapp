INSERT INTO item ( description) VALUES
('Tie'),
('Book'),
('Shirt'),
('Apple'),
('Mango'),
('Pear'),
('Banana'),
('Plum');

INSERT INTO merchant (name) VALUES
('Book Shop'),
('Cloth Shop'),
('Fruit Shop');

INSERT INTO merchant_items (merchant_Id, item_Id, quantity) VALUES
(1,2,10000),
(2,1,20000),
(2,3,5000),
(3,4,100),
(3,5,200),
(3,6,150),
(3,7,50),
(3,8,75);


INSERT INTO user (username, password) VALUES
('nadika', 'hello'),
('john', 'plaintext');
