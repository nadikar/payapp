package com.nadika.mc.assessment.paymentsystem.persistence.repository;

import com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction;
import org.springframework.data.repository.CrudRepository;

/**
 * Implements CRUD operations for the Payment Transactions
 */
public interface TransactionRepository extends CrudRepository<Transaction, Long> {


}
