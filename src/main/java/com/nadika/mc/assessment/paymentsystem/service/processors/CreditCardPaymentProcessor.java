package com.nadika.mc.assessment.paymentsystem.service.processors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Provides the functionality specific to the creditcard payments.
 * This cane be extended to support multiple payment gateways using factory method implementation for the payment gateways
 */
@Service("CreditCard")
@Slf4j
public class CreditCardPaymentProcessor extends AbstractPaymentProcessor {


    /**
     * Processes the Creditcard payments. For the purpose of this assessment failure response will be
     * returned if the amount is negative.
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return Contains the Payment gateway response details
     */
    @Override
    public Response pay(Payment payment) {
        log.debug("Processing payment for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
        Response response = Response.builder()
                .gatewayReference(UUID.randomUUID().toString())
                .success(Boolean.TRUE)
                .build();

        //Simulate payment failures by providing a negative amount
        if (payment.getPaymentRequest().getAmount().compareTo(BigDecimal.ZERO) < 0) {
            response.setSuccess(Boolean.FALSE);
        }
        log.debug("Payment Completed for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
        return response;
    }

    /**
     * Handle any logic needed when the payment is successful. Although there is nothing implemented for the scope
     * of this assessment, introduced it to demonstrate the completeness of the design
     *
     * @param payment  Request contain paymentRequest reference and paymentRequest details
     * @param response Contains the Payment gateway response details
     */
    @Override
    public void handleSuccess(Payment payment, Response response) {
        //Nothing at the moment. But any processing needed after successful
    }


}
