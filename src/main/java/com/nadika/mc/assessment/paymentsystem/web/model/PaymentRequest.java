package com.nadika.mc.assessment.paymentsystem.web.model;

import com.nadika.mc.assessment.paymentsystem.model.CardDetails;
import com.nadika.mc.assessment.paymentsystem.model.Credentials;
import com.nadika.mc.assessment.paymentsystem.model.Currency;
import com.nadika.mc.assessment.paymentsystem.model.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * Payment Request details. All the fields are validated with appropriate validations
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PaymentRequest {
    @NotNull
    private String correlationId;

    @NotNull
    @NumberFormat
    private Long merchantId;

    @NotNull
    @NumberFormat
    private BigDecimal amount;

    @NotNull
    private Currency currency;

    private Credentials credentials;

    @Valid
    private CardDetails cardDetails;

    @Valid
    private List<OrderItem> orderItems;
}
