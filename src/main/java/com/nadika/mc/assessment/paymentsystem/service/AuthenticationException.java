package com.nadika.mc.assessment.paymentsystem.service;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException() {
        super("Unable to Authenticate the user");
    }
}
