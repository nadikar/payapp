package com.nadika.mc.assessment.paymentsystem.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Common response for payment
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentResponse {
    private String correlationId;
    private Long transactionId;
    private String status;
    private String message;
}
