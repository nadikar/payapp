package com.nadika.mc.assessment.paymentsystem.persistence.repository;

import com.nadika.mc.assessment.paymentsystem.persistence.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Implements CRUD operations for the user
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsernameAndPassword(String username, String password);
}
