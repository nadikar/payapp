package com.nadika.mc.assessment.paymentsystem.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;

/**
 * Represent an order item which includes the quantity
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItem {

    @NotNull
    @NumberFormat
    private Long itemId;

    @NotNull
    @NumberFormat
    private int count;
}
