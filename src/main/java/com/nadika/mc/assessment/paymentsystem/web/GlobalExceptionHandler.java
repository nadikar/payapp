package com.nadika.mc.assessment.paymentsystem.web;

import com.nadika.mc.assessment.paymentsystem.service.AuthenticationException;
import com.nadika.mc.assessment.paymentsystem.service.processors.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Handle exceptions globally across the application.
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Constructs validations errors into a list of error messages
     *
     * @param exception The exception occurred
     * @param headers   The http headers
     * @param status    The http status
     * @param request   The request object
     * @return List of validation errors
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        //Get all errors
        List<String> errors = exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getField() + " " + x.getDefaultMessage())
                .collect(Collectors.toList());

        return getErrorResponse(status, errors);
    }

    /**
     * Handles <code>AuthenticationException</code> with useful error message
     *
     * @param exception The exception occurred
     * @param request   The request object
     * @return Useful error message if an <code>AuthenticationException</code> occurred
     */
    @ExceptionHandler(value = {AuthenticationException.class})
    protected ResponseEntity<Object> handleException(
            AuthenticationException exception, WebRequest request) {
        return getErrorResponse(HttpStatus.UNAUTHORIZED, Arrays.asList(exception.getMessage()));

    }

    /**
     * Handles <code>ValidationException</code> with useful error message
     *
     * @param exception The exception occurred
     * @param request   The request object
     * @return Useful error message if an <code>ValidationException</code> occurred
     */
    @ExceptionHandler(value = {ValidationException.class})
    protected ResponseEntity<Object> handleException(
            RuntimeException exception, WebRequest request) {
        return getErrorResponse(HttpStatus.BAD_REQUEST, Arrays.asList(exception.getMessage()));

    }


    /**
     * Constructs a response with useful error details
     *
     * @param status The HTTP status
     * @param errors List of error message
     * @return A response with useful error details
     */
    private ResponseEntity<Object> getErrorResponse(HttpStatus status, List<String> errors) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status);
        body.put("errors", errors);

        return new ResponseEntity<>(body, new HttpHeaders(), status);
    }
}
