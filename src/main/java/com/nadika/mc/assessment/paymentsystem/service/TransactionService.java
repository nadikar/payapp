package com.nadika.mc.assessment.paymentsystem.service;


import com.nadika.mc.assessment.paymentsystem.model.*;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Provides operations to manage payments and transactions
 */
public interface TransactionService {

    /**
     * Processes a payment if the purchased items are available in the inventory and return the transactionId of the
     * persisted paymentRequest. If there is no inventory available the paymentRequest won't be persisted and relevant error
     * will be raised.
     *
     * @param paymentRequest Contains the details of the paymentRequest
     * @return The transactionId, correlationId and status. If an error occurs error message will be returned too
     */
    PaymentResponse pay(PaymentRequest paymentRequest);


    Optional<TransactionDetailsResponse> get(Long transactionId);

    @Data
    @Builder
    class PaymentRequest {
        private String correlationId;

        private Long merchantId;

        private BigDecimal amount;

        private Currency currency;

        private Credentials credentials;

        private CardDetails cardDetails;

        private PayPalDetails payPalDetails;

        private List<OrderItem> orderItems;

    }

    @Data
    @Builder
    class PaymentResponse {
        private String correlationId;
        private Long transactionId;
        private String status;
        private String message;
    }


    @Data
    @Builder
    class TransactionDetailsResponse {
        private long transactionId;

        private String correlationId;

        private String apiUser;

        private BigDecimal amount;

        private String currency;

        private String cardDetails;

        private String inventoryItems;

        private String gatewayReference;

        private Boolean isSuccessful;

        private String gatewayMessage;

        private LocalDateTime processedTime;

        private Long merchantId;
    }
}
