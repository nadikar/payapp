package com.nadika.mc.assessment.paymentsystem.service.processors;

import org.springframework.stereotype.Service;

/**
 * Place holder for additional payment processor for Paypal payment.
 * Introduced to demonstrate the extensibility of the framework
 */
@Service("PayPal")
public class PayPalPaymentProcessor extends AbstractPaymentProcessor {

    /**
     * Currently unsupported
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return
     */
    @Override
    public Response pay(Payment payment) {
        throw new UnsupportedOperationException("Yet to be implemented");
    }

    @Override
    public void handleSuccess(Payment payment, Response response) {
        //Nothing at the moment. But any processing needed after successful
    }
}
