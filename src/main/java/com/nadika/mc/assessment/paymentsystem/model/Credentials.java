package com.nadika.mc.assessment.paymentsystem.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Credentials model. Excluded the password from toString in case if someone accidentally
 * log the class
 */
@Data
@Builder
public class Credentials {

    @NotNull
    private String apiUser;
    @NotNull
    @ToString.Exclude
    private String apiPassword;
}
