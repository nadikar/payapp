package com.nadika.mc.assessment.paymentsystem.web;

import com.nadika.mc.assessment.paymentsystem.service.TransactionService;
import com.nadika.mc.assessment.paymentsystem.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 * Rest controller with entry points to perform payment and retrieve transactions
 */
@RestController
@RequestMapping("/transactions")
@Slf4j
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserService userService;

    /**
     * Performs a payment if the input data is valid. If the credentials are not valid in the request payment will be
     * rejected without any processing
     *
     * @param paymentRequest Request with payment details.
     * @return If the payment is not valid, appropriate error message will be raised. When payment accepted paymentRequest
     * record reference will be returned with the correlationId pass in the request
     */
    @PostMapping(path = "/payment")
    public ResponseEntity<TransactionService.PaymentResponse> payment(@Valid @RequestBody com.nadika.mc.assessment.paymentsystem.web.model.PaymentRequest paymentRequest) {
        log.info("Started Processing Payment. CorrelationId [{}]", paymentRequest.getCorrelationId());
        userService.authenticate(paymentRequest.getCredentials().getApiUser(), paymentRequest.getCredentials().getApiPassword());

        TransactionService.PaymentResponse paymentResponse = transactionService.pay(TransactionService.PaymentRequest.builder()
                .amount(paymentRequest.getAmount())
                .currency(paymentRequest.getCurrency())
                .cardDetails(paymentRequest.getCardDetails())
                .correlationId(paymentRequest.getCorrelationId())
                .merchantId(paymentRequest.getMerchantId())
                .orderItems(paymentRequest.getOrderItems())
                .credentials(paymentRequest.getCredentials())
                .build()
        );
        log.info("Completed Processing Payment. CorrelationId [{}]", paymentRequest.getCorrelationId());
        return new ResponseEntity<>(TransactionService.PaymentResponse.builder()
                .correlationId(paymentResponse.getCorrelationId())
                .transactionId(paymentResponse.getTransactionId())
                .status(paymentResponse.getStatus())
                .build(), HttpStatus.OK
        );

    }

    /**
     * Returns paymentRequest details if found. Otherwise NO_CONTENT error will be returned
     *
     * @param id The Transaction Id
     * @return PaymentRequest details if found. Otherwise NO_CONTENT error will be returned
     */
    @GetMapping(path = "/{id}")
    public ResponseEntity<TransactionService.TransactionDetailsResponse> get(@PathVariable("id") Long id) {

        Optional<TransactionService.TransactionDetailsResponse> response = transactionService.get(id);

        if (response.isPresent()) {
            return new ResponseEntity<>(response.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
