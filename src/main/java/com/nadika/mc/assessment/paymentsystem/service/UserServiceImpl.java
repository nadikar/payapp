package com.nadika.mc.assessment.paymentsystem.service;

import com.nadika.mc.assessment.paymentsystem.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implements operations to manage users
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    /**
     * Authenticates the user based on username and password. If the username and password does not
     * match <code>AuthenticationException</code> will be raised
     *
     * @param username The username
     * @param password The password
     */
    @Override
    public void authenticate(String username, String password) {
        userRepository.findByUsernameAndPassword(username, password)
                .orElseThrow(AuthenticationException::new);
    }
}
