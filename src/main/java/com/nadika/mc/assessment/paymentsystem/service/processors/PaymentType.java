package com.nadika.mc.assessment.paymentsystem.service.processors;

/**
 * Enumeration of supported payment types
 */
public enum PaymentType {

    CREDIT_CARD,
    PAY_PAL
}
