package com.nadika.mc.assessment.paymentsystem;

public class PaymentSystemException extends RuntimeException {

    public PaymentSystemException(String message) {
        super(message);
    }

}
