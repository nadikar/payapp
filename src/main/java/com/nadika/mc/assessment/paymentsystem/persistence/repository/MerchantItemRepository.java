package com.nadika.mc.assessment.paymentsystem.persistence.repository;

import com.nadika.mc.assessment.paymentsystem.persistence.entity.MerchantItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;

/**
 * Provides operations to manipulate MerchantItems
 */
@Repository
public interface MerchantItemRepository extends JpaRepository<MerchantItems, Long> {

    /**
     * Locking the records from reading and writing for the accuracy although there are performance impacts
     *
     * @param merchantId The Merchant Id
     * @param itemIds    The ItemId
     * @return List of matching items
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT merchantItems FROM MerchantItems merchantItems WHERE merchantItems.merchant.id =:merchantId AND merchantItems.item.id IN :itemIds")
    List<MerchantItems> findOrderItems(Long merchantId, List<Long> itemIds);

}
