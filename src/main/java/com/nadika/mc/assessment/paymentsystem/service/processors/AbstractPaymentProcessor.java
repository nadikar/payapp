package com.nadika.mc.assessment.paymentsystem.service.processors;

import com.nadika.mc.assessment.paymentsystem.PaymentSystemException;
import com.nadika.mc.assessment.paymentsystem.model.OrderItem;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.MerchantItems;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.MerchantItemRepository;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Provides common functionality between cll payment processors. Individual payment processors can override the
 * methods as required to extend the functionality
 */
@Slf4j
public abstract class AbstractPaymentProcessor implements PaymentProcessor {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    MerchantItemRepository merchantItemRepository;

    /**
     * If the payment fails the inventory items will be updated by adding the quantities of the current payment order
     * to reverse the paymentRequest.
     *
     * @param payment  The Payment request with the paymentRequest details
     * @param response Contains the Payment gateway response details
     */
    @Override
    @Transactional
    public void handleFailure(Payment payment, Response response) {
        log.debug("Processing payment failure for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
        List<MerchantItems> merchantItems = merchantItemRepository
                .findOrderItems(payment.getPaymentRequest().getMerchantId(), payment.getPaymentRequest().getOrderItems()
                        .stream()
                        .map(OrderItem::getItemId)
                        .collect(Collectors.toList()));

        Map<Long, OrderItem> orderItemMap = payment.getPaymentRequest().getOrderItems()
                .stream()
                .collect(Collectors.toMap(OrderItem::getItemId, oi -> oi));


        merchantItems.forEach(
                merchantItem -> {
                    final int orderCount = orderItemMap.get(merchantItem.getItem().getId()).getCount();
                    merchantItem.setQuantity(merchantItem.getQuantity() + orderCount);
                }
        );
        merchantItemRepository.saveAll(merchantItems);
        log.debug("Reversed the order details");
    }

    /**
     * Updates the paymentRequest record with payment gateway reference, any gateway messages and whether it is successful or not
     *
     * @param payment  The Payment request with the paymentRequest details
     * @param response Contains the Payment gateway response details
     */
    @Override
    public void postProcess(Payment payment, Response response) {
        log.debug("Post Processing payment for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
        Transaction savedTransaction = transactionRepository
                .findById(payment.getTransactionId())
                .orElseThrow(() -> new PaymentSystemException("Unrecoverable Payment System Failure."));
        savedTransaction.setIsSuccessful(response.getSuccess());
        savedTransaction.setGatewayReference(response.getGatewayReference());
        savedTransaction.setGatewayMessage(response.getGatewayMessage());
        transactionRepository.save(savedTransaction);
        log.debug("Post Processing completed for payment with CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
    }

    /**
     * Validates the paymentRequest before processing it. The merchant stock levels are validate here.
     * Please note that the merchant items are locked pessimistically locked here for the accuracy (avoid concurrent updates) but this can
     * adversely affect the performance.
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     */
    @Override
    @Transactional
    public void preProcess(Payment payment) {
        log.debug("Pre Processing payment for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
        List<MerchantItems> merchantItems = merchantItemRepository
                .findOrderItems(payment.getPaymentRequest().getMerchantId(), payment.getPaymentRequest().getOrderItems()
                        .stream()
                        .map(OrderItem::getItemId)
                        .collect(Collectors.toList()));
        //Constructs a map for fater processing
        Map<Long, OrderItem> orderItemMap = payment.getPaymentRequest().getOrderItems()
                .stream()
                .collect(Collectors.toMap(OrderItem::getItemId, oi -> oi));

        if (payment.getPaymentRequest().getOrderItems().size() > merchantItems.size()) {
            throw new ValidationException("Some selected items are not available with the Merchant.");
        }

        merchantItems.forEach(
                merchantItem -> {
                    final int orderCount = orderItemMap.get(merchantItem.getItem().getId()).getCount();
                    if (merchantItem.getQuantity() < orderCount) {
                        throw new ValidationException("Insufficient Stock available.");
                    }
                    merchantItem.setQuantity(merchantItem.getQuantity() - orderCount);
                }
        );
        merchantItemRepository.saveAll(merchantItems);
        log.debug("Pre Processing completed and update quantities for CorrelationId [{}] and TransactionId [{}]",
                payment.getPaymentRequest().getCorrelationId(), payment.getTransactionId());
    }


}
