package com.nadika.mc.assessment.paymentsystem.service;

/**
 * Provides operations to manage users and authentications
 */
public interface UserService {

    /**
     * Authenticates the user based on username and password.
     *
     * @param username The username
     * @param password The password
     */
    void authenticate(String username, String password);
}
