package com.nadika.mc.assessment.paymentsystem.service.processors;

import com.nadika.mc.assessment.paymentsystem.service.TransactionService;
import lombok.Builder;
import lombok.Data;

/**
 * High level interface with operation allowed in a payment processor.
 * Concrete payment processors can be implemented to support each payment instrument type supported by the system.
 */
public interface PaymentProcessor {

    /**
     * Implements functionality performs prior to processing the payment such as validation.
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     */
    void preProcess(Payment payment);


    /**
     * Orchestrate the payment processing flow. Default orchestrate flow as follows
     * 1. preProcess
     * 2. pay
     * 3. postProcess
     * 4. If the payment successful then handleSuccess
     * 5. If the payment unsuccessful then handleFailure
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return The response containing payment gateway reference and indicating the success of the payment
     */
    default Response process(Payment payment) {
        preProcess(payment);
        Response response = pay(payment);

        postProcess(payment, response);

        if (response.getSuccess()) {
            handleSuccess(payment, response);
        } else {
            handleFailure(payment, response);
        }
        return response;
    }

    /**
     * Processes the payment
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return The response containing payment gateway reference and indicating the success of the payment
     */
    Response pay(Payment payment);

    /**
     * Any processing to be done only when the payment is successful will be implemented
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return The response containing payment gateway reference and indicating the success of the payment
     */
    void handleSuccess(Payment payment, Response response);

    /**
     * Any processing to be done only when the payment is failed will be implemented
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return The response containing payment gateway reference and indicating the success of the payment
     */
    void handleFailure(Payment payment, Response response);

    /**
     * Post processing will be implemented in this method irrespective of the status of the payment.
     * Usually clean up logic will be implemented here
     *
     * @param payment Request contain paymentRequest reference and paymentRequest details
     * @return The response containing payment gateway reference and indicating the success of the payment
     */
    void postProcess(Payment payment, Response response);

    /**
     * Payment Request for the payment processor
     */
    @Builder
    @Data
    class Payment {
        private TransactionService.PaymentRequest paymentRequest;
        private Long transactionId;
    }

    /**
     * Payment response containing processed payment details
     */
    @Builder
    @Data
    class Response {
        private String gatewayReference;
        private Boolean success;
        private String gatewayMessage;
    }
}
