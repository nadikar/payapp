package com.nadika.mc.assessment.paymentsystem.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.TransactionRepository;
import com.nadika.mc.assessment.paymentsystem.service.processors.PaymentProcessor;
import com.nadika.mc.assessment.paymentsystem.service.processors.PaymentType;
import com.nadika.mc.assessment.paymentsystem.service.processors.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Implementation of the <code>TransactionService</code> service. Which provides functions to
 * perform payments and retrieve transactions.
 */
@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    Map<PaymentType, PaymentProcessor> paymentProcessors = new HashMap<>();

    @Autowired
    @Qualifier("CreditCard")
    PaymentProcessor creditCardPaymentProcessor;

    @Autowired
    @Qualifier("PayPal")
    PaymentProcessor payPalPaymentProcessor;


    @Autowired
    TransactionRepository transactionRepository;

    /**
     * Construct a map of payment processors soon after the bean gets created
     */
    @PostConstruct
    public void initialise() {
        paymentProcessors.put(PaymentType.CREDIT_CARD, creditCardPaymentProcessor);
        paymentProcessors.put(PaymentType.PAY_PAL, payPalPaymentProcessor);
    }

    /**
     * Processes the payment based on the payment type
     *
     * @param paymentRequest Contains the details of the paymentRequest
     * @return Returns the transactionId, status of the payment.
     */
    @Override
    @Transactional
    public PaymentResponse pay(PaymentRequest paymentRequest) {

        //Capture the paymentRequest irrespective of the further validations as it will be needed for auditing
        Long transactionId = insertTransaction(paymentRequest);
        log.debug("Persisted the transaction with CorrelationId [{}] and TransactionId [{}]",
                paymentRequest.getCorrelationId(), transactionId);

        PaymentType paymentType = getPaymentType(paymentRequest);

        log.debug("Obtained the payment processor for [{}]", paymentType.name());


        PaymentProcessor.Payment payment = PaymentProcessor.Payment.builder()
                .paymentRequest(paymentRequest)
                .transactionId(transactionId)
                .build();

        PaymentProcessor.Response response = paymentProcessors
                .get(paymentType)
                .process(payment);
        log.debug("Processed the Payment. CorrelationId [{}]", paymentRequest.getCorrelationId());

        return PaymentResponse.builder()
                .correlationId(payment.getPaymentRequest().getCorrelationId())
                .transactionId(transactionId)
                .status(response.getSuccess() ? "SUCCESSFUL" : "FAILED")
                .message(null)
                .build();
    }

    /**
     * Retrieves a transaction from the database.
     *
     * @param transactionId The unique id of the transaction
     * @return The transaction details if found
     */
    @Override
    public Optional<TransactionDetailsResponse> get(Long transactionId) {
        return transactionRepository.findById(transactionId)
                .map(entity -> TransactionDetailsResponse.builder()
                        .amount(entity.getAmount())
                        .apiUser(entity.getApiUser())
                        .cardDetails(entity.getCardDetails())
                        .correlationId(entity.getCorrelationId())
                        .currency(entity.getCurrency())
                        .gatewayMessage(entity.getGatewayMessage())
                        .inventoryItems(entity.getOrderItems())
                        .isSuccessful(entity.getIsSuccessful())
                        .processedTime(entity.getProcessedTime())
                        .transactionId(entity.getTransactionId())
                        .gatewayReference(entity.getGatewayReference())
                        .merchantId(entity.getMerchantId())
                        .build());
    }

    /**
     * Inserts a transaction to the database
     *
     * @param paymentRequest The Payment request
     * @return The unique Id of the saved transaction
     */
    protected Long insertTransaction(PaymentRequest paymentRequest) {
        String inventoryJson = paymentRequest.getOrderItems().toString();
        try {
            inventoryJson = new ObjectMapper().writeValueAsString(paymentRequest.getOrderItems());
        } catch (JsonProcessingException jpe) {
            //Safely ignore the JSON as error as this should not occur do to the validations in the controller and
            //ideally should have helper method to construct printable string from list
        }
        com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction entity = com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction.builder()
                .amount(paymentRequest.getAmount())
                .apiUser(paymentRequest.getCredentials().getApiUser())
                .cardDetails(paymentRequest.getCardDetails().toString())
                .correlationId(paymentRequest.getCorrelationId())
                .currency(paymentRequest.getCurrency().name())
                .orderItems(inventoryJson)
                .processedTime(LocalDateTime.now())
                .merchantId(paymentRequest.getMerchantId())
                .build();
        return transactionRepository.save(entity).getTransactionId();
    }

    /**
     * Rudimentary logic to determines the PaymentType based on the payment details.
     *
     * @param paymentRequest The Payment request
     * @return PaymentType based on the payment details
     */
    private PaymentType getPaymentType(PaymentRequest paymentRequest) {
        if (null != paymentRequest.getCardDetails()) {
            return PaymentType.CREDIT_CARD;
        } else if (null != paymentRequest.getCardDetails()) {
            return PaymentType.PAY_PAL;
        } else {
            throw new ValidationException("Either creditcard or paypal details must be provided");
        }
    }
}
