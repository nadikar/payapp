package com.nadika.mc.assessment.paymentsystem.model;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Credit card details model. Sensitive data masked in the toString method.
 */
@Data
@Builder
public class CardDetails {

    @CreditCardNumber
    private String number;

    @NotNull
    @Pattern(regexp = "^(0[1-9]|1[0-2])[0-9][0-9]$", message = "Expiry date should be in MMYY format")
    private String expiryDate;

    @NotNull
    @NumberFormat
    @Size(min = 3, max = 5)
    private String cvv;

    /**
     * Generate toString with masked sensitive data such as card number and CVV
     *
     * @return data elements of the object with masked sensitive fields
     */
    @Override
    public String toString() {
        return "CardDetails{" +
                "number='XXXX XXXX XXX " + number.substring(12) + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", cvv='XXX'}";
    }
}
