package com.nadika.mc.assessment.paymentsystem.model;

/**
 * Consists of allowable currencies in the system
 */
public enum Currency {

    AUD,
    USD,
    GBR
}
