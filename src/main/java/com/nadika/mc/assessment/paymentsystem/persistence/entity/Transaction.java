package com.nadika.mc.assessment.paymentsystem.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Represents the transactions entity
 */
@Data
@Entity(name = "TRANSACTION")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long transactionId;

    @Column(nullable = false)
    private String correlationId;

    @Column(nullable = false)
    private String apiUser;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private String currency;

    @Column(nullable = false)
    private String cardDetails;

    @Column(nullable = false)
    private Long merchantId;

    @Column(nullable = false)
    private String orderItems;

    private String gatewayReference;

    private Boolean isSuccessful;

    private String gatewayMessage;

    @Column(nullable = false)
    private LocalDateTime processedTime;


}
