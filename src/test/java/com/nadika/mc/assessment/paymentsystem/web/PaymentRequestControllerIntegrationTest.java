package com.nadika.mc.assessment.paymentsystem.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.nadika.mc.assessment.paymentsystem.model.CardDetails;
import com.nadika.mc.assessment.paymentsystem.model.Credentials;
import com.nadika.mc.assessment.paymentsystem.model.Currency;
import com.nadika.mc.assessment.paymentsystem.model.OrderItem;
import com.nadika.mc.assessment.paymentsystem.service.TransactionService;
import com.nadika.mc.assessment.paymentsystem.service.UserService;
import com.nadika.mc.assessment.paymentsystem.web.model.PaymentRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Provides integration tests for the <code>TransactionController</code>
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class PaymentRequestControllerIntegrationTest {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    /**
     * Given : Valid input data and initial data available in the database
     * When : Processing a payment
     * Then : It should return Successful response
     */
    @Test
    public void shouldPaymentSuccessfulWithValidData() throws Exception {

        PaymentRequest request = createMockRequest(2L, "nadika", BigDecimal.TEN, "4111111111111111");
        final String requestAsString = new ObjectMapper().writeValueAsString(request);

        this.mockMvc.perform(post("/transactions/payment")
                .content(requestAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESSFUL"));

    }

    /**
     * Given : Initial data available in the database and the request has negative amount
     * When : Processing a payment
     * Then : It should return Failed response
     */
    @Test
    public void shouldPaymentFailWhenAmountNegative() throws Exception {

        PaymentRequest request = createMockRequest(2L, "nadika", new BigDecimal(-10), "4111111111111111");
        final String requestAsString = new ObjectMapper().writeValueAsString(request);

        this.mockMvc.perform(post("/transactions/payment")
                .content(requestAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("FAILED"));

    }

    /**
     * Given : Initial data available in the database and the request has invalid apiUsername
     * When : Processing a payment
     * Then : It should return Unauthorized response
     */
    @Test
    public void shouldPaymentUnauthorizedWhenUserUnavailable() throws Exception {

        PaymentRequest request = createMockRequest(2L, "invaliduser", new BigDecimal(-10), "4111111111111111");
        final String requestAsString = new ObjectMapper().writeValueAsString(request);

        this.mockMvc.perform(post("/transactions/payment")
                .content(requestAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.status").value("UNAUTHORIZED"));

    }

    /**
     * Given : Initial data available in the database and the request has merchantId without stock
     * When : Processing a payment
     * Then : It should return stock unavailable error
     */
    @Test
    public void shouldPaymentThrowValidationErrorWhenMerchantInvalid() throws Exception {

        PaymentRequest request = createMockRequest(1L, "nadika", new BigDecimal(-10), "4111111111111111");
        final String requestAsString = new ObjectMapper().writeValueAsString(request);

        this.mockMvc.perform(post("/transactions/payment")
                .content(requestAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.errors").value("Some selected items are not available with the Merchant."));

    }


    /**
     * Given : There is PaymentRequest available in the database
     * When : Fetching it
     * Then : It should return a valid response
     */
    @Test
    public void shouldGetTransactionReturnValidResponse() throws Exception {

        PaymentRequest request = createMockRequest(2L, "nadika", BigDecimal.TEN, "4111111111111111");
        final String requestAsString = new ObjectMapper().writeValueAsString(request);

        MvcResult result = this.mockMvc.perform(post("/transactions/payment")
                .content(requestAsString)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESSFUL"))
                .andReturn();

        Integer id = JsonPath.read(result.getResponse().getContentAsString(), "$.transactionId");

        this.mockMvc.perform(get("/transactions/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionId").value(id));

    }

    /**
     * Rudimentary load test which execute the payments sequentially. Given more time I would use a ThreadPool and create
     * a runnable task to execute the payments concurrently
     *
     * @throws Exception
     */
    @Test
    public void loadTest() throws Exception {

        for (int i = 0; i < 10; i++) {
            this.shouldPaymentSuccessfulWithValidData();
        }
    }

    private PaymentRequest createMockRequest(final Long merchantId, final String apiUsername, BigDecimal amount, final String cardNumber) {
        return PaymentRequest.builder()
                .amount(amount)
                .cardDetails(CardDetails.builder()
                        .cvv("123")
                        .expiryDate("1123")
                        .number(cardNumber)
                        .build())
                .correlationId(UUID.randomUUID().toString())
                .credentials(Credentials.builder()
                        .apiUser(apiUsername)
                        .apiPassword("hello")
                        .build())
                .currency(Currency.AUD)
                .merchantId(merchantId)
                .orderItems(Arrays.asList(new OrderItem(1L, 2), new OrderItem(3L, 2)))
                .build();
    }
}