package com.nadika.mc.assessment.paymentsystem.service;

import com.nadika.mc.assessment.paymentsystem.model.CardDetails;
import com.nadika.mc.assessment.paymentsystem.model.Credentials;
import com.nadika.mc.assessment.paymentsystem.model.Currency;
import com.nadika.mc.assessment.paymentsystem.model.OrderItem;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.TransactionRepository;
import com.nadika.mc.assessment.paymentsystem.service.processors.PaymentProcessor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Qualifier;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for <code>TransactionServiceImpl</code>
 * Only minimal unit tests were written due to time constraint
 */
@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    @Spy
    @InjectMocks
    TransactionServiceImpl transactionService;

    @Mock
    @Qualifier("CreditCard")
    PaymentProcessor creditCardPaymentProcessor;

    @Mock
    @Qualifier("PayPal")
    PaymentProcessor payPalPaymentProcessor;


    @Mock
    TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        transactionService.initialise();
    }

    @AfterEach
    public void tearDown() {
        transactionService = null;
    }

    /**
     * Given: when valid payment details provided
     * When : accepting payments
     * Then : It should persist the transaction, process the payment and return the transactionId
     */
    @Test
    public void shouldPaySuccessfulForCreditcard() {
        Mockito.doReturn(1L).when(transactionService).insertTransaction(Mockito.any());
        Mockito.doReturn(PaymentProcessor.Response.builder()
                .gatewayReference("mockGateWayRef")
                .success(Boolean.TRUE)
                .build()).when(creditCardPaymentProcessor).process(Mockito.any());
        TransactionService.PaymentRequest paymentRequest = createMockRequest(BigDecimal.TEN);
        TransactionService.PaymentResponse response = transactionService.pay(paymentRequest);

        assertNotNull(response);
        assertEquals("SUCCESSFUL", response.getStatus());
        assertEquals(paymentRequest.getCorrelationId(), response.getCorrelationId());
        assertEquals(1L, response.getTransactionId());

        Mockito.verify(transactionService, Mockito.times(1)).insertTransaction(paymentRequest);
        Mockito.verify(creditCardPaymentProcessor, Mockito.times(1)).process(Mockito.any());
    }


    /**
     * Given: valid transactionId
     * When : retrieving transactions
     * Then : It should return transaction details
     */
    @Test
    public void shouldGetTransactionSuccessful() {

        Transaction transaction = Transaction.builder()
                .transactionId(10L)
                .amount(BigDecimal.TEN)
                .cardDetails("mockJson")
                .correlationId(UUID.randomUUID().toString())
                .apiUser("mockUser")
                .currency(Currency.AUD.name())
                .merchantId(1L)
                .orderItems("mockOrder")
                .processedTime(LocalDateTime.now())
                .gatewayReference("mockRef")
                .build();
        Mockito.doReturn(Optional.of(transaction)).when(transactionRepository).findById(Mockito.any(Long.class));

        Optional<TransactionService.TransactionDetailsResponse> response = transactionService.get(10L);

        assertTrue(response.isPresent());
        assertEquals(10L, response.get().getTransactionId());
        //Can write assertions to check all data elements
    }


    private TransactionService.PaymentRequest createMockRequest(BigDecimal amount) {
        return TransactionService.PaymentRequest.builder()
                .amount(amount)
                .cardDetails(CardDetails.builder()
                        .cvv("123")
                        .expiryDate("1123")
                        .number("4111111111111111")
                        .build())
                .correlationId(UUID.randomUUID().toString())
                .credentials(Credentials.builder()
                        .apiUser("mockUser")
                        .apiPassword("hello")
                        .build())
                .currency(Currency.AUD)
                .merchantId(1L)
                .orderItems(Arrays.asList(new OrderItem(1L, 2), new OrderItem(3L, 2)))
                .build();
    }

}