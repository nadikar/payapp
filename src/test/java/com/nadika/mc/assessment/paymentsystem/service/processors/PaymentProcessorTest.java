package com.nadika.mc.assessment.paymentsystem.service.processors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit tests for <code>PaymentProcessor</code>
 */
@ExtendWith(MockitoExtension.class)
class PaymentProcessorTest {

    //construct a concrete class for testing
    static class TestPaymentProcessor implements PaymentProcessor {

        @Override
        public void preProcess(Payment payment) {

        }

        @Override
        public Response pay(Payment payment) {
            return Response.builder().build();
        }

        @Override
        public void handleSuccess(Payment payment, Response response) {

        }

        @Override
        public void handleFailure(Payment payment, Response response) {

        }

        @Override
        public void postProcess(Payment payment, Response response) {

        }
    }

    @InjectMocks
    @Spy
    TestPaymentProcessor paymentProcessor;

    /**
     * Given: Payment request data available
     * When : processing payment with successful response
     * Then : It should call all methods but handleFailure
     */
    @Test
    public void testProcessWhenPaymentSuccessful() {
        Mockito.doReturn(PaymentProcessor.Response.builder()
                .success(Boolean.TRUE)
                .build()).when(paymentProcessor).pay(Mockito.any());
        paymentProcessor.process(Mockito.any());

        Mockito.verify(paymentProcessor, Mockito.times(1)).preProcess(Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).pay(Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).postProcess(Mockito.any(), Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).handleSuccess(Mockito.any(), Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.never()).handleFailure(Mockito.any(), Mockito.any()); //should never get called
    }

    /**
     * Given: Payment request data available
     * When : processing payment with failed response
     * Then : It should call all methods but handleSuccess
     */
    @Test
    public void testProcessWhenPaymentFailed() {
        Mockito.doReturn(PaymentProcessor.Response.builder()
                .success(Boolean.FALSE)
                .build()).when(paymentProcessor).pay(Mockito.any());
        paymentProcessor.process(Mockito.any());

        Mockito.verify(paymentProcessor, Mockito.times(1)).preProcess(Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).pay(Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).postProcess(Mockito.any(), Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.times(1)).handleFailure(Mockito.any(), Mockito.any());
        Mockito.verify(paymentProcessor, Mockito.never()).handleSuccess(Mockito.any(), Mockito.any()); //should never get called
    }

}