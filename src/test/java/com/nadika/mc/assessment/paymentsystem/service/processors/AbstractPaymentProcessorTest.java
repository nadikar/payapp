package com.nadika.mc.assessment.paymentsystem.service.processors;

import com.nadika.mc.assessment.paymentsystem.model.OrderItem;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.Item;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.Merchant;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.MerchantItems;
import com.nadika.mc.assessment.paymentsystem.persistence.entity.Transaction;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.MerchantItemRepository;
import com.nadika.mc.assessment.paymentsystem.persistence.repository.TransactionRepository;
import com.nadika.mc.assessment.paymentsystem.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit tests for <code>AbstractPaymentProcessor</code>
 */
@ExtendWith(MockitoExtension.class)
class AbstractPaymentProcessorTest {

    //construct a concrete class for testing
    static class TestPaymentProcessor extends AbstractPaymentProcessor {


        @Override
        public Response pay(Payment payment) {
            return Response.builder().build();
        }

        @Override
        public void handleSuccess(Payment payment, Response response) {

        }
    }

    @InjectMocks
    TestPaymentProcessor testPaymentProcessor;

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    MerchantItemRepository merchantItemRepository;


    /**
     * Given: Items and quantity available for the merchant
     * When : pre processing payments
     * Then : It should deduct the quantity from the merchant inventory
     */
    @Test
    public void shouldPreProcessSuccessful() {

        List<MerchantItems> merchantItems = Arrays.asList(buildMerchantItem(1L, 1L, 1L, 10),
                buildMerchantItem(2L, 2L, 1L, 20));
        Mockito.doReturn(merchantItems).when(merchantItemRepository).findOrderItems(Mockito.any(), Mockito.any());
        Mockito.doReturn(merchantItems).when(merchantItemRepository).saveAll(Mockito.any());

        PaymentProcessor.Payment payment = buildPayment(1L, 1L, 5, 2L, 2);

        testPaymentProcessor.preProcess(payment);

        Map<Long, Integer> itemQuantity = merchantItems.stream()
                .collect(Collectors.toMap(merchItms -> merchItms.getItem().getId(), merchItms -> merchItms.getQuantity()));

        //item 1L->initial 10, order 5
        assertEquals(5, itemQuantity.get(1L));
        //item 2L->initial 20, order 2
        assertEquals(18, itemQuantity.get(2L));

    }

    /**
     * Given: Items and quantity available less than the order for the merchant
     * When : pre processing payments
     * Then : It should throw a ValidationException
     */
    @Test
    public void shouldPreProcessThrowExceptionWhenNoStock() {

        List<MerchantItems> merchantItems = Arrays.asList(buildMerchantItem(1L, 1L, 1L, 2),
                buildMerchantItem(2L, 2L, 1L, 2));
        Mockito.doReturn(merchantItems).when(merchantItemRepository).findOrderItems(Mockito.any(), Mockito.any());

        PaymentProcessor.Payment payment = buildPayment(1L, 1L, 5, 2L, 2);

        assertThrows(ValidationException.class, () -> testPaymentProcessor.preProcess(payment));
    }


    /**
     * Given: The payment failed by the payment gateway
     * When : handling failures
     * Then : It should add the quantity back to the inventory
     */
    @Test
    public void shouldHandleFailureSuccessful() {

        List<MerchantItems> merchantItems = Arrays.asList(buildMerchantItem(1L, 1L, 1L, 10),
                buildMerchantItem(2L, 2L, 1L, 20));
        Mockito.doReturn(merchantItems).when(merchantItemRepository).findOrderItems(Mockito.any(), Mockito.any());
        Mockito.doReturn(merchantItems).when(merchantItemRepository).saveAll(Mockito.any());

        PaymentProcessor.Payment payment = buildPayment(1L, 1L, 5, 2L, 2);

        testPaymentProcessor.handleFailure(payment, PaymentProcessor.Response.builder().build());

        Map<Long, Integer> itemQuantity = merchantItems.stream()
                .collect(Collectors.toMap(merchItms -> merchItms.getItem().getId(), merchItms -> merchItms.getQuantity()));

        //item 1L->initial 10, order 5
        assertEquals(15, itemQuantity.get(1L));
        //item 2L->initial 20, order 2
        assertEquals(22, itemQuantity.get(2L));

    }


    /**
     * Given: The payment failed by the payment gateway
     * When : handling failures
     * Then : It should add the quantity back to the inventory
     */
    @Test
    public void shouldPostProcessSuccessful() {

        Transaction transaction = Transaction.builder()
                .build();

        Mockito.doReturn(Optional.of(transaction)).when(transactionRepository).findById(Mockito.any());
        Mockito.doReturn(transaction).when(transactionRepository).save(Mockito.any());

        PaymentProcessor.Response response = PaymentProcessor.Response.builder()
                .success(Boolean.TRUE)
                .gatewayReference("mockRef")
                .gatewayMessage("mockMessage")
                .build();
        PaymentProcessor.Payment payment = buildPayment(1L, 1L, 5, 2L, 2);

        testPaymentProcessor.postProcess(payment, response);

        assertEquals(Boolean.TRUE, transaction.getIsSuccessful());
        assertEquals("mockRef", transaction.getGatewayReference());
        assertEquals("mockMessage", transaction.getGatewayMessage());

    }


    private PaymentProcessor.Payment buildPayment(Long merchantId, Long itemId1, int itemId1Qty, Long itemId2, int itemId2Qty) {
        return PaymentProcessor.Payment.builder()
                .paymentRequest(TransactionService.PaymentRequest.builder()
                        .merchantId(1L)
                        .orderItems(Arrays.asList(OrderItem.builder()
                                        .itemId(itemId1)
                                        .count(itemId1Qty)
                                        .build(),
                                OrderItem.builder()
                                        .itemId(itemId2)
                                        .count(itemId2Qty)
                                        .build()
                        ))
                        .build())
                .build();
    }

    private MerchantItems buildMerchantItem(Long id, Long itemId, Long merchantId, int qty) {
        return MerchantItems.builder()
                .id(id)
                .item(Item.builder()
                        .id(itemId)
                        .build())
                .merchant(Merchant.builder()
                        .id(merchantId)
                        .build())
                .quantity(qty)
                .build();
    }
}